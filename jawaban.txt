1.Membuat Database

create database myshop;

2.Membuat Table di Dalam Database

a.create table users(
    -> id int auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> paswword varchar(255),
    -> primary key(id)
    -> );


b.create table items(
    -> id int auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );


c.create table categories(
    -> id int auto_increment,
    -> kategori varchar(255),
    -> primary key(id)
    -> );


3.Memasukkan Data pada Table

a.insert into users values(null,"John Doe","john@doe.com","john1602");
  insert into users values(null,"Jane Doe","jane@doe.com","jane1602");
b.insert into categories(kategori) values ("gadget"),("cloth"),("men"),("women"),("branded");
c.insert into items(id,name,description,price,stock,category_id)
    -> values(null,"Sumsangb50","hape keren dari merek sumsang",4000000,100,1),
    -> (null,"Uniklooh","baju keren dari brand ternama",500000,50,2),
    -> (null,"IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4.Mengambil Data dari Database

a. Mengambil data users

select id,name,email from users;

b. Mengambil data items

-select * from items where price > 1000000;
-select * from items where name like '%watch%';

c. Menampilkan data items join dengan kategori

select items.name, items.description, items.price, items.stock, items.category_id, categories.kategori from items join
    -> categories on items.category_id = categories.id;
5 Mengubah Data dari Database

update items set price=2500000 where id = 1;
